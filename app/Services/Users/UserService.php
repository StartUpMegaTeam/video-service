<?php

namespace App\Services\Users;

use App\Enums\ChannelEnum;
use App\Models\Channel;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Throwable;

class UserService
{
    public function getUser(int $id): User
    {
        return User::findOrFail($id);
    }

    /**
     * @param array $data
     * @return User
     * @throws Throwable
     */
    public function store(array $data): User
    {
        try {
            DB::beginTransaction();

            if (!$user = User::create($data)) {
               return false;
            }

            $this->channelCreate($user->name, $user->id);

            DB::commit();

            return $user;

        } catch (Throwable $ex) {
            DB::rollBack();
            throw $ex;
        }
    }

    protected function channelCreate($username, $userId)
    {
        DB::table('channels')->insert([
            'name' => $username,
            'description' => '',
            'user_id' =>  $userId,
            'selected' => ChannelEnum::SELECTED
        ]);
    }

//    /**
//     * @param int $id
//     * @param array $data
//     * @return User
//     * @throws BusinessLogicException
//     */
//    public function update(int $id, array $data): User
//    {
//        $user = $this->getUser($id);
//
//        if (!$user->update($data)) {
//            throw new BusinessLogicException(trans('exception.user.update'));
//        }
//
//        return $user;
//    }
//
//    /**
//     * @param User $user
//     * @param int $status
//     * @return bool
//     * @throws BusinessLogicException
//     */
//    public function updateStatus(User $user, int $status): bool
//    {
//        if (!$user->update(['status' => $status])) {
//            throw new BusinessLogicException(trans('exception.user.update_status'));
//        }
//
//        return true;
//    }
//
//    /**
//     * @throws BusinessLogicException
//     */
//    public function archiving(User $user): bool
//    {
//        if ($user->deleted_at !== null) {
//            if (!$user->restore()) {
//                throw new BusinessLogicException(trans('exception.company.update_status'));
//            }
//        } else {
//            if (!$user->delete()) {
//                throw new BusinessLogicException(trans('exception.company.update_status'));
//            }
//        }
//
//        return true;
//    }
}

