<?php

namespace App\Services\Channels;

use App\Enums\ChannelEnum;
use App\Models\Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ChannelService
{
    public function getChannels()
    {
        return Channel::where('user_id', Auth::user()->id)->get();
    }

    public function getSelectChannel()
    {
        return Channel::where('user_id', Auth::user()->id)->where('selected',ChannelEnum::SELECTED)->first();
    }

    public function changeStatus()
    {
        $channels = $this->getChannels();

        foreach ($channels as $channel) {
            $channel->selected = ChannelEnum::UNSELECTED;
            $channel->save();
        }
    }

    public function updateStatus($id)
    {
        $channel = Channel::findOrFail($id);
        $channel->update(['selected' => ChannelEnum::SELECTED]);

        return Storage::url($channel->avatar);
    }

    public function upload(Request $request, Channel $channel):bool
    {
        $file = $request->file('avatar');
        $uploadFolder = 'public/avatar/' .$channel->id . '/';
        $fileName = $file->hashName();
        Storage::putFileAs($uploadFolder, $file, $fileName);

        $channel->update(['avatar' => 'avatar/' . $channel->id . '/' . $fileName]);

         return true;
    }
}
