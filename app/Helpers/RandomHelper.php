<?php

namespace App\Helpers;

use Str;

class RandomHelper
{
    /**
     * Return random string with the length of $size.
     *
     * @param int $size length of string
     * @param bool $onlyDigits to use only digits
     * @return string
     */
    public static function generateRandomString(int $size, bool $onlyDigits = false): string
    {
        if ($size < 1) {
            $size = 1;
        }

        return $onlyDigits ? substr(str_shuffle("0123456789"), 0, $size) : Str::random($size);
    }
}
