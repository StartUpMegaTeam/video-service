<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $picture
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class Category extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'description',
        'picture',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];



    public function setSlugAttribute(){
        $this->attributes['slug'] = Str::slug($this->name , "-");
    }

}
