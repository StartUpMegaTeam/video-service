<?php

namespace App\Models;

use App\Filters\Filterable;
use App\Filters\QueryFilter;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property int $channel_id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $tags
 * @property string $video_path
 * @property string $preview_path
 * @property string $subtitles_path
 * @property int $visits
 * @property boolean $archived
 *
 * @method static Builder available(Post $post)
 * @method static Builder|Post filter(QueryFilter $filter)

 * @property-read Channel[]|Collection $channels
 *
 * @property Carbon|null $archived_at
 * @property Carbon|null $created_at
 */
class Post extends Model
{
    use HasApiTokens, HasFactory, Notifiable,Filterable;

    protected $fillable = [
        'country_code',
        'channel_id',
        'category_id',
        'title',
        'description',
        'tags',
        'video_path',
        'preview_path',
        'subtitles_path',
        'visits',
        'archived',
        'archived_at',
        'deleted_at',
    ];

    public function channels(): BelongsTo
    {
        return $this->belongsTo(Channel::class, 'channel_id', 'id');
    }

    public function categories(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @throws Exception
     */
    public function historyCreated(): string
    {
        $data = Carbon::now()->diffForHumans($this->created_at);

        return $data;
    }

    public function visits(): string
    {
        $tousen = 1000;
        $million = 1000000;
        $milliard = 1000000000;

        $countVisits = $this->visits;

        if ($countVisits > $milliard)
        {
            $countVisits = $countVisits / $milliard;
            $visits = $countVisits.' миллиард.';
        }elseif ($countVisits > $million)
        {
            $countVisits = $countVisits / $million;
            $visits = $countVisits.' мил.';
        }elseif ($countVisits > $tousen){

            $countVisits = $countVisits / $tousen;
            $visits = $countVisits.' тыс.';
        }

        return $visits;
    }


    public function setVideoPath($file)
    {
        $destination_path = 'video/' . $this->id;
        // Generate a filename.
        $filename = md5($file . time()) . '.mp4';

        // Store the image on disk.
        Storage::put($destination_path . '/' . $filename, $file);

        // Save the path to the database
        $this->video_path = $destination_path . '/' . $filename;
    }


    public function scopeAvailable(Builder $query): Builder
    {
        return $query->where('archived', '=', 0);
    }


}
