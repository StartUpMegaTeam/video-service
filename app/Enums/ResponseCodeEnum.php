<?php

namespace App\Enums;

final class ResponseCodeEnum
{
    const OK = 0;
    const ERROR = 1;
    const WARNING = 2;
}
