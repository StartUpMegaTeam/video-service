<?php

namespace App\Enums;

class NotificationStatusEnum
{
    const READ = 1;
    const UNREAD = 2;
    const SHOWN = 3;

    const STATUSES = [
        self::READ => 'Read',
        self::UNREAD => 'Unread',
        self::SHOWN => 'Shown',
    ];

}

