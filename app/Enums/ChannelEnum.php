<?php

namespace App\Enums;

class ChannelEnum
{
    const UNSELECTED = 0;
    const SELECTED = 1;
}
