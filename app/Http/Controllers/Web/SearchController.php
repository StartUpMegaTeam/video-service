<?php

namespace App\Http\Controllers\Web;

use App\Filters\Video\VideoFilter;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\View\View;

class SearchController extends Controller
{
    /**
     * @param VideoFilter $filter
     * @return View
     */
    public function index(VideoFilter $filter): View
    {
        $posts = Post::available()->filter($filter)->get();

        return view('home.search')->with(['posts' => $posts]);
    }
}
