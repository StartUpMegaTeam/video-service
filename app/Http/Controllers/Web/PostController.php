<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Request\Post\PostStoreRequest;
use App\Models\Post;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;

class PostController extends Controller
{

    public function store(PostStoreRequest $request): bool
    {
        return true;
    }

    /**
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        $post = Post::find($id)->first();
        $postsGroup =Post::get()
            ->groupBy('category_id')
            ->map(function($deal) {
            return $deal->take(10);
        });;

        return view('post.show')->with(['post' => $post, 'postsGroup' => $postsGroup]);
    }

}
