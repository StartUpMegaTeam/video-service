<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Request\Channel\ChannelRequest;
use App\Models\Channel;
use App\Services\Channels\ChannelService;
use Illuminate\View\View;

class ChannelController extends Controller
{
    public function __construct(protected ChannelService $service)
    {
    }

    public function index(): View
    {
        return view('channel.index')->with([
            'channels' => $this->service->getChannels(),
            'select_channel' => $this->service->getSelectChannel()
        ]);
    }

    public function store(ChannelRequest $request)
    {
        $this->service->changeStatus();
        $chanel = Channel::create($request->validated());
        $this->service->upload($request, $chanel);

        return $chanel->id
            ? redirect()->route('web.channel.index')
            : redirect()->back()->withInput();
    }

    public function updateStatus(int $id)
    {
        $this->service->changeStatus();
        return $this->service->updateStatus($id);
    }
}
