<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(): View
    {
        $posts = Post::where('archived', '=', 0)->get();
        $categories = Category::where('archived', '=', 0)->get();

        return view('home.index')->with(['posts' => $posts,'categories' => $categories]);
    }
}
