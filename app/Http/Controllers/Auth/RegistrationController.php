<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Request\Auth\LoginRequest;
use App\Http\Request\Auth\RegistrationStoreRequest;
use App\Models\Channel;
use App\Services\Users\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Throwable;

class RegistrationController extends Controller
{
    public function __construct(protected UserService $service)
    {
    }


    /**
     * @return View
     */
    public function create(): View
    {
        return view('auth.register');
    }


    /**
     * @throws Throwable
     */
    public function store(RegistrationStoreRequest $request): RedirectResponse
    {

        return $this->service->store($request->validated())
            ? redirect()->route('web.login') //->with(['success' => trans('message.user.create.success')])
            : redirect()->back()->withInput();//->with(['error' => 'message.user.create.error'])->withInput();
    }
}
