<?php

namespace App\Http\Request\Channel;

use App\Enums\ChannelEnum;
use App\Services\Channels\ChannelService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ChannelRequest extends FormRequest
{
    public function rules()
    {

        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'avatar' => ['mimetypes:image/jpeg,image/png'],
        ];
    }

    public function validated($key = null, $default = null): array
    {

        return [
            'name' => $this->input('name'),
            'description' => $this->input('description'),
            'user_id' => Auth::user()->id,
            'selected' => ChannelEnum::SELECTED,
        ];
    }
}
