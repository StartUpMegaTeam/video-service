<?php

namespace App\Http\Request\Auth;


use Illuminate\Foundation\Http\FormRequest;

class RegistrationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'string','unique:users,email'],
            'password' => [
                'required',
                'string',
                'min:' . config('app.user.password.min'),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'password.string' => 'Invalid character',
            'password.required' => 'Password is required!',
            'password.min' => 'Minimum password 8 characters'
        ];
    }
}

