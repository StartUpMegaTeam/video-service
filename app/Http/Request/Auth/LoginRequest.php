<?php

namespace App\Http\Request\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string'],
            'password' => [
                'required',
                'string',
                'min:' . config('app.user.password.min'),
            ],
        ];
    }

    public function messages()
    {
        return [
            'password.string' => 'Invalid character',
            'password.required' => 'Password is required!',
            'password.min' => 'Minimum password 8 characters'
        ];
    }


    /**
     * Attempt to authenticate the request's credentials.
     *
     * @throws ValidationException
     */
    public function authenticate(): void
    {
        $user = $this->getCurrentUser();

        if (!$user) {
            throw ValidationException::withMessages([
                'email' => 'Неверный Email или пароль',
            ]);
        }else{
            Auth::login($user);
        }

    }

    /**
     * @return User|null
     */
    public function getCurrentUser(): User|null
    {
        return User::where(['email' => $this->input('email')])->first();
    }
}

