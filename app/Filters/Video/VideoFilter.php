<?php

namespace App\Filters\Video;

use App\Filters\QueryFilter;


class VideoFilter extends QueryFilter
{
    public function q(string $q)
    {
        $this->builder->where(function ($query) use ($q) {
            $query->where('title', 'like', '%' . $q . '%')
                ->orWhere('description', 'like', '%' . $q . '%')
                ->orWhereHas('categories', function ($query) use ($q) {
                    $query->where('name', 'like', '%' . $q . '%');
                });
        });
    }
}
