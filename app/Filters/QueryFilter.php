<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use ReflectionMethod;
use ReflectionParameter;

abstract class QueryFilter
{
    protected Request $request;
    protected Builder $builder;

    protected array $multipleFields = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        $mf = $this->getMultipleFields();

        foreach ($this->fields() as $field => $value) {
            $method = Str::camel($field);
            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], (array)$value);
            }
        }

        foreach ($mf as $func => $args){
            $method = Str::camel($func);
            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], (array)$args);
            }
        }

        return $this->builder;
    }

    public function withRelation(Builder $builder, string $relation): Builder
    {
        $this->builder = $builder;
        $method = Str::camel('with_' . $relation);

        if (method_exists($this, $method)) {
            $rm = new ReflectionMethod($this, $method);

            $params = $rm->getParameters();
            $fields = $this->fields();

            $paramsArray = array_map(function (ReflectionParameter $item) use ($fields) {
                return $fields[Str::snake($item->name)] ?? null;
            }, $params);

            call_user_func_array([$this, $method], $paramsArray);
        }

        return $this->builder;
    }

    protected function getMultipleFields(): array
    {
        $fromRequest = $this->fields();

        $values = [];
        foreach ($this->multipleFields as $multipleFieldKey => $multipleFieldFields) {
            $values[$multipleFieldKey] = collect($multipleFieldFields)->mapWithKeys(function ($field) use ($fromRequest) {
                return [$field => $fromRequest[$field] ?? null];
            })->toArray();
        }

        return $values;
    }

    protected function fields(): array
    {
        return array_filter(
            array_map('trim', $this->request->all()),
            function ($value) {
                return $value !== '';
            }
        );
    }
}

