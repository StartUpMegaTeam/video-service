<?php


namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class QueryOrder
{
    protected ?string $defaultColumn = null;
    protected string $defaultDirection = 'asc';

    protected array $defaultColumns = [];

    protected Request $request;
    protected Builder $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        $column = $this->getColumn();
        $direction = $this->getDirection();

        if (is_null($column)) {
            return $this->builder;
        }

        if (in_array($column, $this->defaultColumns)) {
            $this->applyDefaultOrderBy($column, $direction);

        } else {
            $method = Str::camel($column);

            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], []);
            }
        }

        return $this->builder;
    }

    protected function getColumn(): ?string
    {
        $orderBy = $this->request->input('order_by');
        return is_null($orderBy)
            ? (is_null($this->defaultColumn)
                ? null
                : $this->defaultColumn)
            : $orderBy;
    }

    protected function getDirection(): string
    {
        $sort = $this->request->input('sort');
        return is_null($sort)
            ? $this->defaultDirection
            : $sort;
    }

    protected function applyDefaultOrderBy(string $column, string $direction)
    {
        $this->builder->orderBy($column, $direction);
    }
}

