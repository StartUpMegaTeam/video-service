<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * @method static Builder filter(QueryFilter $filter)
 *
 */
trait Filterable
{
    public function scopeFilter(Builder $builder, QueryFilter $filter)
    {
        $filter->apply($builder);
    }
}
