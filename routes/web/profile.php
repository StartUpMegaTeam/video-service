<?php

use App\Http\Controllers\Web\ProfileController;
use Illuminate\Support\Facades\Route;

Route::prefix('profile')->name('profile.')->middleware(['auth:admin'])->group(function () {

    Route::get('', [ProfileController::class, 'edit'])
        ->name('edit');

    Route::put('{id}', [ProfileController::class, 'update'])
        ->name('update');
});
