<?php

use App\Http\Controllers\Web\SearchController;
use Illuminate\Support\Facades\Route;

Route::prefix('search')->name('search.')->group(function () {

    Route::get('', [SearchController::class, 'index'])
        ->name('index');
});
