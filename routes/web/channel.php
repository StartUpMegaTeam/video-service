<?php

use App\Http\Controllers\Web\ChannelController;
use Illuminate\Support\Facades\Route;

Route::prefix('channel')->name('channel.')->group(function () {

    Route::get('', [ChannelController::class, 'index'])
        ->name('index');

    Route::post('', [ChannelController::class, 'store'])
        ->name('store');

    Route::get('/update-status/{id}', [ChannelController::class, 'updateStatus'])
        ->name('updateStatus');
});
