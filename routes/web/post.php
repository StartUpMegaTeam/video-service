<?php

use App\Http\Controllers\Web\PostController;
use Illuminate\Support\Facades\Route;

Route::prefix('post')->name('post.')->group(function () {

    Route::get('', [PostController::class, 'index'])
        ->name('index');

    Route::get('{id}/edit', [PostController::class, 'edit'])
        ->name('edit');

    Route::get('{id}/show', [PostController::class, 'show'])
        ->name('show');

    Route::get('create', [PostController::class, 'create'])
        ->name('create');

    Route::post('', [PostController::class, 'store'])
        ->name('store');

    Route::put('{id}', [PostController::class, 'update'])
        ->name('update');

    Route::delete('{id}', [PostController::class, 'destroy'])
        ->name('destroy');

    Route::post('{id}/restore', [PostController::class, 'restore'])
        ->name('restore');
});
