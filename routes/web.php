<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('')->name('web.')->group(function () {
    require __DIR__ . '/web/home.php';
    require __DIR__ . '/web/auth.php';
    require __DIR__ . '/web/profile.php';
    require __DIR__ . '/web/post.php';
    require __DIR__ . '/web/channel.php';
    require __DIR__ . '/web/search.php';
});
