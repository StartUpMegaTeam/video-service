import './bootstrap';
import Vue from "vue";

window.Vue = require('vue');

Vue.component('channel-main', require('./components/channel/ChannelMain.vue').default);
Vue.component('channel-change', require('./components/channel/ChannelChange.vue').default);
Vue.component('channel-create', require('./components/channel/ChannelCreate.vue').default);
Vue.component('video-create', require('./components/video/VideoCreate.vue').default);
Vue.component('header-main', require('./components/layouts/Header.vue').default);
Vue.component('home-main', require('./components/home/Home.vue').default);
Vue.component('search-main', require('./components/search/Search.vue').default);
Vue.component('post-show', require('./components/post/PostShow.vue').default);

const app = new Vue({
    el: '#app'
});
