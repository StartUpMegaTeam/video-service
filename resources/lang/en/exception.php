<?php

return [
    'page_not_found' => 'Entity not found.',
    'save_error' => 'Error while saving the data.',
    'movement_status_unable' => 'Unable to change movement status from current status.',

];
