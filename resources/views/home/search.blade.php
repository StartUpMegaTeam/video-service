@extends('master.goest')

@section('title')
    Поиск
@endsection

@section('content')
    <search-main :posts="{{$posts}}" route_show="{{route('web.post.show',":id")}}"></search-main>
@endsection
