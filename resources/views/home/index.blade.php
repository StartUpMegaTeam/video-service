@extends('master.goest')

@section('title')
    Главная
@endsection

@section('content')
    <home-main :categoties="{{$categories}}" :posts="{{$posts}}" route_show="{{route('web.post.show',":id")}}"></home-main>
@endsection
