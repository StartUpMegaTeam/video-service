@extends('master.studio')

@section('title')
    Студия
@endsection

@section('content')

    <channel-main
        route_index="{{route('web.home.index')}}"
        route_create_channel="{{route('web.channel.store')}}"
        :channels="{{$channels}}"
        select_channel="{{$select_channel->avatar}}"
    ></channel-main>
@endsection


