<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="#" type="image/svg"> <!--Логотип-->

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Inter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,latin-ext">

    <link rel="stylesheet" href="{{ asset('vendor/plyr-master/dist/plyr.css')}}">
    <link rel="stylesheet" href={{ asset('css/app.css') }}>

    <title>@yield('title')</title>


    <script type="text/javascript" src="{{ asset('js/vendor/jquery/jquery-3.6.0.js') }}"></script>

    @stack('assets')
</head>
<body>
<div id="app">
    <main>
        @yield('content-master')
    </main>
</div>

<script src="{{asset('vendor/plyr-master/dist/plyr.js')}}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    // const players = Plyr.setup('.js-player');

    const players = Array.from(document.querySelectorAll('.js-player')).map(p => new Plyr(p, {
        settings: [
            'captions',
            'quality',
            'speed',
            'loop'
        ],
        controls: [
            'play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'settings', 'pip', 'airplay', 'fullscreen'
        ],
        quality: {
            default: 1080, options: [4320, 2880, 2160, 1440, 1080, 720, 576, 480, 360, 240]
        }
    }));

</script>

</body>
</html>
