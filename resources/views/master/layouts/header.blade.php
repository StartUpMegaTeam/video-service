@php
    $notifications = \App\Models\Notification::where('user_id',
    \Illuminate\Support\Facades\Auth::user()->id)->where('status',
    \App\Enums\NotificationStatusEnum::UNREAD)->orderBy('created_at','DESC')->get();
    $notificationsCount = count($notifications);
@endphp

<header-main
    route_home="{{route('web.home.index')}}"
    route_search="{{route('web.search.index')}}"
    route_channel="{{route('web.channel.index')}}"
    route_login="{{route('web.login')}}"
    route_logout="{{route('web.logout')}}"
    route_register="{{route('web.register')}}"
    auth="{{\Illuminate\Support\Facades\Auth::user()}}"
    notifications="{{$notifications}}"
    notifications_count="{{$notificationsCount}}"
>
</header-main>
