@php
    $navItems = [
        ['label' => 'Movements', 'url' => route('admin.movements.index'), 'icon' => asset('img/user/movements.svg'), 'prefix' => 'movements*', ],
        ['label' => 'Aircraft Profiles', 'url' => route('admin.aircraft.index'), 'icon' => asset('img/aircraft/aircraft.svg'),'prefix' => 'aircrafts*',],
        ['label' => 'Interactive Map', 'url' => route('admin.map.index'), 'icon' => asset('img/user/map.svg'),'prefix' => 'map*', ],
        ['label' => 'Module Status', 'url' => route('admin.modules.index'), 'icon' => asset('img/user/modules.svg'),'prefix' => 'modules*',],

        ['label' => 'Data', 'icon' => asset('img/sidebar/data.svg'), 'prefix' => ['statistics*', 'incidents*' ], 'subItems' => [
            ['label' => 'Statistics', 'url' => route('admin.statistics.index'), 'icon' => asset('img/user/statistics.svg'), 'prefix' => 'statistics*',],
            ['label' => 'Incidents History', 'url' => route('admin.incidents.index'), 'icon' => asset('img/user/incidents.svg'), 'prefix' => 'incidents*',],
        ]],

        ['label' => 'Settings', 'icon' => asset('img/user/modules.svg'),'prefix' => ['users*','settings*','offline-operations*',], 'subItems' =>
            array_merge([
                ['label' => 'Users', 'url' => route('admin.users.index'), 'icon' => asset('img/user/users.svg'), 'prefix' => 'users*', ],
                ['label' => 'Offline Operations', 'url' => route('admin.offline_operations.index'), 'icon' => asset('img/user/settings.svg'), 'prefix' => 'offline-operations*', ],
            ], Auth::user()->roleIn(\App\Enums\UserRoleEnum::ROOT)
                ? []
                : [['label' => 'Module Settings', 'url' => route('admin.settings.index'), 'icon' => asset('img/user/settings.svg'),'prefix' => 'settings*', ],]
            )
        ],

        ['label' => 'Setup','icon' => asset('img/sidebar/setup.svg'), 'prefix' => ['aircraft-models*', 'tugs*', 'tug-models*', 'locations*',], 'subItems' => [
           ['label' => 'Aircraft Models', 'url' => route('admin.aircraft-models.index'), 'icon' => asset('img/aircraft_models/aircraft_models.svg'), 'prefix' => 'aircraft-models*', ],
           ['label' => 'Tugs', 'url' => route('admin.tugs.index'), 'icon' => asset('img/tugs/tugs_ic.svg'), 'prefix' => 'tugs*',],
           ['label' => 'Tug Models', 'url' => route('admin.tug-models.index'), 'icon' => asset('img/tugs/tug_models.svg'), 'prefix' => 'tug-models*',],
           ['label' => 'Facility Maps', 'url' => route('admin.location.index'), 'icon' => asset('img/user/location.svg'), 'prefix' => 'locations*',],
        ]],
    ];
@endphp

<div class="sidebar">
    <div class="sidebar__logo">
        <img class="sidebar__logo-img" src="{{ asset('img/user/logo.svg') }}" alt="Sidebar Logo">
    </div>
    <ul class="sidebar__nav">
        @foreach($navItems as $navItem)
            @if(empty($navItem['subItems']))
                <a class="js-active-sidebar_1 sidebar__a {{ Request::is($navItem['prefix']) ? 'sidebar__a--active' : '' }}"
                   href="{{ $navItem['url'] }}">
                    <li>
                        <img class="sidebar__img" src="{{ $navItem['icon'] }}" alt="{{ $navItem['label'] }}">
                        <p>{{ $navItem['label'] }}</p>
                    </li>
                </a>
            @else
                <div class="nav-group__list js-nav-group-accordion">
                    <div class="nav-group__accordion {{ Request::is(...$navItem['prefix']) ? 'active' : '' }}">
                        <div class="nav-group__accordion-header">
                            <div class="nav-group__accordion-header-label">
                                <img class="sidebar__img" src="{{ $navItem['icon'] }}"
                                     alt="{{ $navItem['label'] }}">
                                <p>{{ $navItem['label'] }}</p>
                            </div>
                            <img class="nav-group__accordion-header-img" src="{{ asset('img/sidebar/accardeon.svg') }}">
                        </div>
                        <div class="nav-group__accordion-content">
                            @foreach($navItem['subItems'] as $subItem)
                                <a class="js-active-sidebar_1 sidebar__a  sidebar__a-group  sidebar__a-group--border {{ Request::is($subItem['prefix']) ? 'sidebar__a--active' : '' }}"
                                   href="{{ $subItem['url'] }}">
                                    <li>
                                        <img class="sidebar__img" src="{{ $subItem['icon'] }}"
                                             alt="{{ $subItem['label'] }}">
                                        <p>{{ $subItem['label'] }}</p>
                                    </li>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </ul>
</div>

<script>
    $('.js-nav-group-accordion > *').click(function () {
        $(this).toggleClass('active');
        $('.js-nav-group-accordion > *').not(this).removeClass('active');
    });
</script>
