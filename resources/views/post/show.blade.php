@extends('master.goest')

@section('title')
    Просмотр
@endsection

@section('content')
    <post-show :posts_group="{{$postsGroup}}" :post_show="{{$post}}"  route_show="{{route('web.post.show',":id")}}"></post-show>
@endsection

