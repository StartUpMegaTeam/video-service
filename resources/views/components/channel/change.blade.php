<channel-change>
    channels="{{ json_encode($channels) }}"
    route_form="{{ route('web.channel.store') }}"
    method_post = @method('POST')
    csrf_token ={{ csrf_token() }}
</channel-change>


