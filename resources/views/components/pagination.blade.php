<div class="pagination">
    @if ($paginator->hasPages())
        <ul class="pagination">
            @if (!$paginator->onFirstPage())
                <li class="pagination__button pagination__button--back">
                    <a href="{{ $paginator->previousPageUrl() . '&per_page=' . $paginator->perPage() }}"
                       tabindex="0" class="pagination__button-link">
                        <img src="{{ asset('img/pagination/button-back.svg') }}" alt="">
                    </a>
                </li>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="pagination__button pagination__button--gap">
                        <a class="pagination__button-link">
                            {{ $element }}
                        </a>
                    </li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="pagination__button pagination__button--active">
                                <a class="pagination__button-link">
                                    {{ $page }}
                                </a>
                            </li>
                        @else
                            <li class="pagination__button pagination__button--page ">
                                <a href="{{ $url . '&per_page=' . $paginator->perPage() }}"
                                   class="pagination__button-link">
                                    {{ $page }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class="pagination__button pagination__button--next">
                    <a href="{{ $paginator->nextPageUrl() . '&per_page=' . $paginator->perPage() }}"
                       class="pagination__button-link">
                        <img src="{{ asset('img/pagination/button-next.svg') }}">
                    </a>
                </li>
            @endif
        </ul>
    @endif
</div>


