<video-create>
    route_form="{{ route('web.post.store') }}"
    method_post = @method('POST')
</video-create>

<script>
    $('#post-title').on('input', function () {
        if ($('#post-title').val() === '') {
            $('.js-post-create-title').html('Заголовок')
        } else {
            $('.js-post-create-title').html($('#post-title').val());
        }
    })

    $('#post-description').on('input', function () {
        if ($('#post-description').val() === '') {
            $('.js-post-create-description').html('Описание')
        } else {
            $('.js-post-create-description').html($('#post-description').val());
        }
    })

    $(document).on("change", ".file_multi_video", function(evt) {
        var $source = $('#video_here');
        $source[0].src = URL.createObjectURL(this.files[0]);
        $source.parent()[0].load();
    });

    $(document).on("change", ".file_multi_preview", function(evt) {
        var $preview = $('.video_preview');
        $preview.attr('poster' , URL.createObjectURL(this.files[0]));
        $preview.parent()[0].load();
    });
</script>
