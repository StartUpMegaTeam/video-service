@extends('master.auth')

@section('content')

    <div class="register">
        <form class="register__form" action="{{route('web.register.store')}}"
              method="POST">
            @method('POST')
            @csrf

            <div class="input  @error("name") invalid @enderror">
                <label class="input__label" for="name">Имя</label>
                <input id="name" class="input-text" type="text" name="name" placeholder="Введите ваше имя">
                @error("name")
                <span class="invalid-feedback" role="alert">
              <p class="invalid-feedback--p">{{ $message }}</p>
              </span>
                @enderror
            </div>

            <div class="input  @error("name") invalid @enderror">
                <label class="input__label" for="email">Email</label>
                <input id="email" class="input-text" type="email" name="email" placeholder="Введите Email">
                @error("email")
                <span class="invalid-feedback" role="alert">
              <p class="invalid-feedback--p">{{ $message }}</p>
              </span>
                @enderror
            </div>

            <div class="input  @error("name") invalid @enderror">
                <label class="input__label" for="password">Пароль</label>
                <input id="password" class="input-text" type="password" name="password" placeholder="Введите пароль">
                @error("password")
                <span class="invalid-feedback" role="alert">
              <p class="invalid-feedback--p">{{ $message }}</p>
              </span>
                @enderror
            </div>


            <div class="register__buttons">
                <button class="btn register__submit" type="submit">Продолжить</button>
                <a href="{{route('web.home.index')}}" class="btn register__back">Отмена</a>
            </div>
        </form>
    </div>

@endsection
