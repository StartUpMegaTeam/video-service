<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('country_code', 2)->nullable();
            $table->unsignedBigInteger('channel_id');
            $table->unsignedBigInteger('category_id');
            $table->string('title', 191);
            $table->text('description');
            $table->string('video_path', 191);
            $table->string('preview_path', 191)->nullable();
            $table->string('subtitles_path', 191)->nullable();
            $table->text('tags')->nullable();
            $table->integer('visits')->unsigned()->nullable()->default('0');
            $table->boolean('archived')->nullable()->default('0');
            $table->timestamp('archived_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['channel_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::dropIfExists('posts');
    }
}
