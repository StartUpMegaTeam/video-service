<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->command->info('Channel created!');

        $this->call(CategoryTableSeeder::class);
        $this->command->info('Category created!');

        $this->call(PostTableSeeder::class);
        $this->command->info('Users created!');
    }
}
