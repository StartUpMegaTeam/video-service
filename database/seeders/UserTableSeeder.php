<?php

namespace Database\Seeders;

use App\Enums\ChannelEnum;
use App\Models\Channel;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;

    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < self::NUM_ITEMS; $i++) {
            $user = User::create([
                'name' => "admin" . $i,
                'email' => 'admin' . $i . '@admin' . $i,
                'password' => '123qweR%',

            ]);

            Channel::create([
                'name' => $faker->name,
                'description' => '',
                'user_id' => $user->id,
                'selected' => ChannelEnum::SELECTED
            ]);
        }
    }

}
