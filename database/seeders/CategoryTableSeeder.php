<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;
    const CATEGORIES=['Музыка','Природа','Животные','Картинки','Погода','Авто','Дома','Игры','Анимация','Футбол'];

    public function run()
    {
        $faker = Faker::create();

        foreach (self::CATEGORIES as $category) {
            Category::create([
                'parent_id' => Null,
                'name' => $category,
                'description' => 'video/1/test.jpg',
                'picture' => '',
                'seo_title' => $faker->text,
                'seo_description' => $faker->text,
                'seo_keywords' => $faker->text,
            ]);
        }
    }

}
