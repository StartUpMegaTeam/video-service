<?php

namespace Database\Seeders;

use App\Models\Post;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PostTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;

    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < self::NUM_ITEMS; $i++) {

            Post::create([
                'channel_id' => 1,
                'category_id' => 1,
                'video_path'=>$faker->randomElement(['video/1/test.mp4']),
                'subtitles_path'=>'',
                'title' => $faker->text(100),
                'description' =>  $faker->text(100),
                'tags' => $faker->text,
                'visits' => 0,
            ]);
        }
    }

}
